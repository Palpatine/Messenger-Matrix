Hierbei handelt es sich um eine eigenhändig modifizierte Version der Messenger-Matrix des Kuketz-Blogs (https://media.kuketz.de/blog/messenger-matrix/messenger-matrix.html). Dabei wurden sämtliche Datenschutzfeindliche Messenger entfernt. Die Bewertung ist subjektiv. 

Hinweise: Der Messenger Tox verfügt in den Desktopversionen (qTox, uTox, Toxygen) durchaus über die Möglichkeit, Video- und Sprachanrufe zu tätigen. Der Vorgänger von aTox war Antox. 

Für den durchschnittlichen Anwender wird oftmals die Verwendung von Signal empfohlen. Allerdings ist dies ein zentralisierter Dienst und für die Registrierung wird eine Telefonnummer benötigt. Zudem sitzen sie in der Amazon Cloud und sämtliche Kommunikation läuft über amerikanische Tech-Konzerne wie Amazon, Microsoft und Google (https://www.kuketz-blog.de/signal-jegliche-kommunikation-erfolgt-ueber-tech-giganten-wie-amazon-microsoft-google-und-cloudflare/). 

Mitarbeit am Projekt ist ausdrücklich erwünscht!    